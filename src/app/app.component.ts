import { Component } from '@angular/core';
import { TradeService } from './services/trade.service';
import { TradePricesService } from './services/trade-prices.service';
import { SubmitTradeService } from './services/submit-trade.service';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tradeApp';
  sub:Subscription

  tradeTypeInput = false;
  tradeType = "Buy";
  tickerInput: string = 'TSLA';
  amountInput = 0;
  price = 0;

  allTrades;
  priceHistory:any;
  options:any;
  tradeData: any = {
    ticker: "TSLA",
    status: "Hold",
    date: "01/01/2020",
    currentPrice: 0,
    sellPrice: 0,
    buyPrice: 0
  }


  constructor(private trade: TradeService,
    private tradeAnalyse: TradePricesService,
    private tradePoster: SubmitTradeService) { 
      this.sub= interval(2000).subscribe((x =>{
        this.updateTrades();
    }));
    }


  getStockData() {
    console.log("Getting stock data...")
    this.tradeAnalyse.getTradePrices(this.tickerInput)
      .subscribe((response) => { this.tradeData = response })

    this.tradeAnalyse.getTradePriceHistory(this.tickerInput)
      .subscribe((response) => { this.priceHistory = response })

    this.genGraph();
  }

  updatePrice() {
    this.price = this.amountInput * this.tradeData.currentPrice;
  }

  updateTradeType() {
    if(this.tradeTypeInput) {
      this.tradeType = "Buy";
    } else {
      this.tradeType = "Sell";
    }
  }

  submitTrade() {
    let data = {
      ticker: this.tickerInput,
      amount: this.amountInput,
      tradeType: this.tradeType,
      price: this.price
    }
    console.log(data);
    this.tradePoster.postTrade(data)
      .subscribe((r) => { console.log(r) })

    this.updateTrades()
  }

  updateTrades() {
    this.trade.getTrades()
    .subscribe((response) => { this.allTrades = response })
  }

  genGraph(): void {
    const xAxisData = [];
    const data = [];

    console.log(this.priceHistory);

    let priceData = this.priceHistory.price_data;
    console.log(priceData);

    for (let i = 0; i < 100; i++) {
      xAxisData.push(priceData[i].date);
      data.push(priceData[i].price);
    }

    this.options = {
      legend: {},
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'bar',
          type: 'bar',
          data: data,
        },
      ],
    };
  }

  ngOnInit(): void {
    this.updateTrades()

    /*this.tradeAnalyse.getTradePrices(this.tickerInput)
      .subscribe((response2) => { this.tradeData = response2 })*/

    this.getStockData();
  }
}
