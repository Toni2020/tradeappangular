import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
// for error handling in Http calls
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubmitTradeService {
  placeholderURL = 'http://localhost:8083/api/trade'
  constructor(private http: HttpClient) { }
  
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      return of(result as T);
    };
  }

  // POST
  postTrade(data): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json' 
      })
    };
    return this.http.post(`${this.placeholderURL}`, data, httpOptions)
      .pipe(
        catchError(this.handleError('doPOST', data))
      );
  }

}