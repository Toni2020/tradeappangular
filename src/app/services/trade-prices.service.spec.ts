import { TestBed } from '@angular/core/testing';

import { TradePricesService } from './trade-prices.service';

describe('TradePricesService', () => {
  let service: TradePricesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TradePricesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
