import { TestBed } from '@angular/core/testing';

import { SubmitTradeService } from './submit-trade.service';

describe('SubmitTradeService', () => {
  let service: SubmitTradeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubmitTradeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
