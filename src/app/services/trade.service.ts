import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TradeService {

  constructor(private http:HttpClient) { }

  getTrades() {
    let fullURL="http://localhost:8083/api/trade"
    return this.http.get(fullURL);
  }
}
