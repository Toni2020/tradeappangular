import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TradePricesService {

  constructor(private http: HttpClient) { }

  getTradePrices(ticker) {
    let url = `http://localhost:5000/${ticker}`
    console.log(url);
    return this.http.get(url);
  }

  getTradePriceHistory(ticker) {

    let url = 'https://agez3b6adi.execute-api.eu-west-1.amazonaws.com/default/simplePriceFeed2?ticker='
    let params = '&num_days='

    let fullURL = `${url}${ticker}${params}100`
    console.log(fullURL);
    return this.http.get(fullURL);
  }
}
